alter table product
  ADD COLUMN weight DECIMAL (8,2);

create table product_photos (
  id VARCHAR(36),
  id_product VARCHAR(36) NOT NULL,
  url VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (id_product, url)
);